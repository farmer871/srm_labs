# 使用 SRM 進階選項

## [目的]
SRM 除了提供在數據中心之間進行故障轉移和恢復虛擬機的功能外，還包括許多高級功能，可用於增強解決方案的恢復能力。

以下將探索這些功能：
- 虛擬機恢復屬性
- 故障轉移期間調整虛擬機 IP 位址
- 虛擬機啟動選項
- vSphere Replication進階選項 ([深入淺出 - vSphere Replication搭配使用vSAN](./04_simple.md#vsphere-replication搭配使用vsan))
- 使用VMware SRM 遷移工作負載
- SRM 配置組態管理 ([深入淺出 - SRM組態管理](./04_simple.md#srm組態管理))

## [步驟大綱]

[虛擬機恢復屬性](#虛擬機恢復屬性)
- [確認SRM及複寫狀態](#確認srm及複寫狀態)
- [選擇復原計劃](#選擇復原計劃)
- [選擇被保護的虛擬機器](#選擇被保護的虛擬機器)
- [虛擬機器復原屬性快速瀏覽](#虛擬機器復原屬性快速瀏覽)

[虛擬機啟動選項](#虛擬機啟動選項)
- [選取復原計畫](#選取復原計畫)
- [復原優先順序](#復原優先順序)
- [機器啟動選項](#機器啟動選項)
- [機器相依性](#機器相依性)
- [將虛擬機sales-web-01加入保護群組](#將虛擬機sales-web-01加入保護群組)
- [驗證復原計劃](#驗證復原計劃)



## [執行步驟]

### 虛擬機恢復屬性

#### 確認SRM及複寫狀態
在進行相關測試前，先確認 SRM 連線及複寫服務狀態。

> 點選 **Site Pair** > **Summary** 顯示站點配對摘要。

![ov001.ong](./pics/03/ov/001.png)

> 點選 **Site Pair** > **Configure** > **Replication Servers** 顯示複寫主機狀態。

主要站點 RegionA

![ov002.ong](./pics/03/ov/002.png)

復原站點 RegionB

![ov003.ong](./pics/03/ov/003.png)

> 點選 **Replications** > **Outgoing** 檢視虛擬機複寫狀態。

目前所有虛擬機複寫機制運作正常。

![ov004.ong](./pics/03/ov/004.png)

#### 選擇復原計劃

> 點選 **Recovery Plans** ，選擇 **Sales Application Recovery Plan**。

選擇其中一項復原計劃，我們將快速瀏覽復原計劃中可提供的虛擬機恢復屬性。

![ov005.ong](./pics/03/ov/005.png)

#### 選擇被保護的虛擬機器
> 點選 **···** 功能選單，選擇 **Virtual Machines**，檢視復原計劃中被保護的虛擬機器。

![ov006.ong](./pics/03/ov/006.png)

選擇復原計劃中其中一組虛擬機器，可直接點選上方 **Configure Recovery** 或在虛擬機器欄位按下滑鼠右鍵，顯示功能選單後選擇。

![ov007.ong](./pics/03/ov/007.png)

#### 虛擬機器復原屬性快速瀏覽

- Priority Groups
- VM Dependencies
- Shutdown Action
- Startup Action
- Pre-Power On Steps
- Post-Power On Steps

![ov008.ong](./pics/03/ov/008.png)

**Priority Group**

![ov009.ong](./pics/03/ov/009.png)

**VM Dependencies**

![ov010.ong](./pics/03/ov/010.png)

**Shutdown Action**

![ov011.ong](./pics/03/ov/011.png)

**Startup Action**

![ov012.ong](./pics/03/ov/012.png)

**Pre Power On Steps**

![ov013.ong](./pics/03/ov/013.png)
![ov014.ong](./pics/03/ov/014.png)

**Post Power On Steps**

![ov015.ong](./pics/03/ov/015.png)
![ov016.ong](./pics/03/ov/016.png)

### 遷移時IP變更

Due to budget constraints, the networking team was unable to extend their IP network across both Region A and Region B.  Therefore, during recovery, it is a requirement that the IP addresses of each VM being recovered are modified to match the network environment at Region B.

In this lesson, we will customize the IP addresses to be used for the Sales virtual machines in the event that they need to be recovered to the Region B data center due to a planned migration of disaster.

#### 調整虛擬機器IP選項

![ov017.ong](./pics/03/ov/017.png)






IPv4 Address: 192.168.220.200
Subnet Mask: 255.255.255.0
Default Gateway: 192.168.220.1


#### Sit Network Mappings

Select Network Mappings in the Navigation Pane.
Verify vcsa-01a.corp.local is selected.
Select the VM-RegionA01-vDS-COMP network.
Click Add


    For the RegionA01 Site, enter the following for Subnet:  192.168.120.0/24
    For the RegionB01 Site, enter the following for Subnet:  192.168.220.0/24
    Gateway:  192.168.220.1
    DNS addresses;  192.168.110.10
    DNS suffixes:  corp.local
    Select the ADD button when complete



### 虛擬機啟動選項
我們將以 **Sales Application 保護群組**中的虛擬機器作為測試及演示。
完成以下配置選項：

| 虛擬主機    | 復原優先順序<br>(Priority Group) | 啟動選項<br>(Startup Action) | 相依性<br>(VM Dependencies) |
| ----------- | :---: | :--- | :---: |
| sales-web-01 | 4 | - Power on<br>- VMware tools: Yes, 5 mins<br>- Additional Delay: 3 mins | sales-app-01 |
| sales-app-01 | 4 | - Power on<br>- VMware tools: Yes, 5 mins<br>- Additional Delay: No | sales-db-01 |
| sales-db-01 | 4 | - Power on<br>- VMware tools: Yes, 5 mins<br>- Additional Delay: No | none |

#### 選取復原計畫
> 點選 **Recovery Plans**，選擇 **Sales Application Recovery Plan** 復原計畫。

> 點選 `‧‧‧` 功能展開，選擇 **Virtual Machines**。

![001.png](./pics/03/001.png)

目前僅有兩個虛擬機器 **sales-app-01** 與 **sales-db-01**。根據復原計劃的需求，我們還必須增加一個虛擬機器 **sales-web-01** 至現有的保護群組 **Sales Application Protection Group** 與復原計劃 **Sales Application Recovery Plan**。

![002.png](./pics/03/002.png)

有關 sales-web-01 的部分，後續再進行處理吧。

#### 復原優先順序

復原計劃中的所有虛擬機器的**復原優先順序(Priority)層級預設為 3**。可根據復原計畫需求提高或降低虛擬機器的復原優先順序。

> 優先順序最高為 1，最低為 5。

**復原優先順序決定了虛擬機器關閉和開啟電源的順序**。

**注意** 若變更虛擬機器的優先順序，則會將**新的優先順序套用到包含此虛擬機器的所有復原計劃中**。

虛擬機器在復原站台上執行時，使用 VMware Tools 活動訊號進行探索，可確保指定優先順序的所有虛擬機器都在執行中，然後才啟動下一個優先順序的虛擬機器。

基於此原因，**必須在受保護的虛擬機器上安裝 VMware Tools**。

我們先以單一虛擬機器作為設定目標。

> 選擇單一虛擬機器 sales-db-01。

> 點選上方 **Configure Recovery**。

或

> 在目標虛擬機器**按右鍵**出現功能選單，選擇 **Configure Recovery**。

![003.png](./pics/03/003.png)


> 在 **Recovery Properties** 功能頁籤中，點選 **Priority Group**，將預設值 `3(Medium)` 調整為 `4(Low)`。

![004.png](./pics/03/004.png)

#### 機器啟動選項
復原計畫中可設定復原期間復原站台上虛擬機器的啟動及關閉方式。

**關閉動作(Shutdown Action)**

| 選項 | 說明 |
| --- | --- |
| 關閉客體作業系統，然後再關閉電源 | 正常關閉虛擬機器，然後再關閉其電源。並可設定關閉作業的逾時期間。<br><br>將逾時期間設為 0 等同於 [**關閉電源**] 選項。此選項要求 VMware Tools 正在虛擬機器上執行。<br><br>**逾時到期時將關閉虛擬機器的電源**。如果逾時到期時虛擬機器的作業系統尚未完成其關閉工作，可能會導致資料遺失。**對於需要較長時間才可正常關閉的大型虛擬機器，請設定足夠長的關閉電源逾時。** |
| 關閉電源 | 關閉虛擬機器電源，但不關閉客體作業系統。|

**啟動動作(Startup Action)**

| 選項 | 說明 |
| --- | --- |
| 開啟電源 | 開啟復原站台上虛擬機器的電源。 |
| 不要開啟電源 | 復原虛擬機器，但不要開啟其電源。|

**等待 VMware Tools 核取方塊**
- 啟動動作為**開啟電源(Power on)**時，才為可用選項。
- 勾選該選項，復原計畫會等到 VMware Tools 在開啟虛擬機器電源後啟動才會繼續。

**額外延遲(Additional Delay)**
- 啟動動作為**開啟電源(Power on)**時，才為可用選項。
- 勾選該選項，會在**執行開啟電源後的步驟(Post Power on Steps)和啟動相依虛擬機器(Starting dependent VMs)之前的延遲時間**
- **應用場景** 指定開啟虛擬機器電源後的額外延遲時間，以允許另一個虛擬機器相依的應用程式啟動

> 這裡就啟用 **開啟電源** 並 **等待 VMware Tools** 復原屬性。

![005.png](./pics/03/005.png)

虛擬機器 sales-db-01 復原屬性調整完成後，點選 **OK** 完成。

#### 機器相依性
所謂機器相依性，就是復原計劃執行時會在啟動含相依性的虛擬機器之前，先啟動其他虛擬機器所依存的虛擬機器。

在目前我們設定的場景來看， **sales-app-01 相依 sales-db-01** 的話， **sales-db-01 會先進行開機，然後才會將 sales-app-01 開機**。

設定機器相依性，有以下條件：
- 含相依性的虛擬機器和所相依的虛擬機器必須處於同一復原計畫。
- 含相依性的虛擬機器和所相依的虛擬機器必須具有相同的復原優先順序。

且相依性設定若失效，將有以下結果：
- SRM 無法啟動虛擬機器所依存的虛擬機器，**復原計劃會繼續，但會出現警告**。
- 將虛擬機器設定為相依於較低優先順序群組中的虛擬機器，則 SRM 會覆寫相依性。簡單來說，就是會忽略該相依性設定，且 **復原優先順序的權重大於虛擬機相依**。
-
所以讓我們思考以下問題：

```
  * sales-app-01(priority:3) 相依 sales-db-01(priority:4) 條件下，若設定 sales-app-01 的 VM 相依的復原屬性，會產生什麼問題？
  * 上述問題該如何處理？
  * 如果 acct-app-01 與 sales-app-01 相依的話，兩台虛擬機器並不存在同一保護群組，請問可以設定相依性的復原屬性嗎？
```

讓我們試著設定 sales-app-01 的復原屬性。

> 點選 **VM Dependencies** ，選擇 **View VM dependencies** 展開下拉選單，選擇 **View all**。

![006.png](./pics/03/006.png)

我們可以發現以下幾點事實：
1. 相依性虛擬機器僅有 sales-db-01 可以選擇。
 > 因為......

2. 虛擬機器 sales-db-01 圖示出現驚嘆號。
 > 因為......

所以我們要使得假設復原設定生效，必須先調整 sales-app-01 的復原優先順序。

![007.png](./pics/03/007.png)

到此為止，**sales-app-01** 和 **sales-db-01** 的設定應該都完成了！

#### 將虛擬機sales-web-01加入保護群組
> 點選 **Replications** 功能頁籤。

從 SRM 管理介面檢視 sales-web-01 虛擬機器，並未在複寫保護清單當中。

![008.png](./pics/03/008.png)

接著依序下列步驟將其加入 Sales Application 保護群組。
> 點選 **+NEW** 新增複寫虛擬機器 sales-web-01。

![009.png](./pics/03/009.png)

手動選擇複寫主機 **vr-01b**。
> 點選 **Manaually select vSphere Replication Server**，選擇 **vr-01b**。

![010.png](./pics/03/010.png)

> 選擇目的端存儲位置 **RegionB01-LOCAL-ESX01B**

![011.png](./pics/03/011.png)

---

**注意**

這裡 **Target datastore** 還可有另一個選擇，採用 **vSAN** 。相關設定請參考

[深入淺出 - vSphere Replication搭配使用vSAN](./04_simple.md)

---
接著調整複寫設定，這裏採用預設組態即可。若爲生產環境，請按照實際需求進行參數組態。

![012.png](./pics/03/012.png)

符合測試場景，將其加入既有保護群組 **Sales Appication Protection Group**。

![013.png](./pics/03/013.png)

確認組態資訊，點選 **FINISH** 完成。

![014.png](./pics/03/014.png)

SRM 便會開始進行複寫並加入保護群組。

![015.png](./pics/03/015.png)

複寫狀態完成。

![016.png](./pics/03/016.png)

回到保護群組確認所保護的虛擬機器數量及狀態。

![017.png](./pics/03/017.png)

請自行完成虛擬機器 **sales-web-01** 復原計劃啟動設定吧。

- 優先順序: 4
- 虛擬機相依: sales-app-01
- 啟動選項:
  - 開機
  - VMware Tools偵測
  - 額外延遲: 3 分鐘

#### 驗證復原計劃
現在應該對於如何執行復原計劃測試很熟悉了吧？！

![018.png](./pics/03/018.png)

復原計劃進行測試時，也同步觀察復原啟動流程是否按照設定進行！

觀察開機優先順序的虛擬機器。首先由 **sales-db-01** 開機，並等待 VMware Tools 偵測完畢，才進行下一臺虛擬機器開機。

![019.png](./pics/03/019.png)

從 vSphere Web Client 也可以觀察到虛擬機器開機順序。

![020.png](./pics/03/020.png)

虛擬機器 **sales-app-01** 接續開機。

![021.png](./pics/03/021.png)

從 vSphere Web Client 也可以觀察，已完成兩部虛擬機器開機。

![022.png](./pics/03/022.png)

虛擬機器 **sales-web-01** 最後進行開機。

![023.png](./pics/03/023.png)

從 vSphere Web Client 觀察到，所有被保護的虛擬機器已在復原站點完成開機。

![024.png](./pics/03/024.png)

**sales-web-01** 在啟動流程中額外設定延遲時間 3 分鐘，從復原步驟中也可觀察到。

![025.png](./pics/03/025.png)

還記得 SRM 可提供復原計劃歷程紀錄報告？！從報告紀錄的時間戳記更能驗證復原計劃按照設定程序進行且完成。

![026.png](./pics/03/026.png)


## [深入淺出](./04_simple.md)


## [參考]

- VMware Hands-on Labs - **HOL-2005-01-SDC**: VMware Site Recovery Manager - Data Center Migration and Disaster Recovery
   - [啟動實驗](http://labs.hol.vmware.com/HOL/catalogs/lab/6228 "啟動實驗")
   - [實驗指引](https://docs.hol.vmware.com/HOL-2020/hol-2005-01-sdc_html_en/ "實驗指引")

- [設定復原計畫](https://docs.vmware.com/tw/Site-Recovery-Manager/8.3/com.vmware.srm.admin.doc/GUID-A1ADBBCC-850D-42EE-BBD5-652204A98470.html)

---
[[回到主頁]](./README.md)

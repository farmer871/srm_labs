# VMware Site Recovery Manager
Site Recovery Manager (SRM) 是VMware領先業界的災難復原 (DR) 管理解決方案，專為在災難發生時將停機時間縮到最短而設計。

**備註** 之後將以 **SRM** 簡稱**開啟災難復原管理軟體(Site Recovery Manager)

### [VMware SRM 官方產品說明](https://www.vmware.com/tw/products/site-recovery-manager.html)

#### [學習準備](./99_prepare_lab.md)
#### [使用 SRM 演練虛擬機器故障移轉](./01_failover_vm_basic.md)
#### [使用 SRM 執行災難復原計畫](./02_recovery_plan_basic.md)
#### [使用 SRM 進階選項](./03_srm_features_advanced.md)
#### [深入淺出](./04_simple.md)


## [參考]

- VMware Hands-on Labs - **HOL-2005-01-SDC**: VMware Site Recovery Manager - Data Center Migration and Disaster Recovery
   - [啟動實驗](http://labs.hol.vmware.com/HOL/catalogs/lab/6228 "啟動實驗")
   - [實驗指引](https://docs.hol.vmware.com/HOL-2020/hol-2005-01-sdc_html_en/ "實驗指引")

---

# LAB 準備項目

- [新增 Accounting 保護群組及復原計畫](#新增-accounting-保護群組及復原計畫)
- [新增 Site-Wide Recovery Plan](#新增-site-wide-recovery-plan)
- [增加sales-web-01複寫保護](#增加sales-web-01複寫保護)
- [新增虛擬主機putty連線設定](#新增虛擬主機putty連線設定)
- [加入命令檔](#加入命令檔)
- [調整Sales Application Recovery Plan](#調整sales-application-recovery-plan)


## 新增 Accounting 保護群組及復原計畫
1.Name and direction
  - Name: **Accounting Protection Group**
  - Description: **Accounting department protection group**

2.Type
  - **Individual VMs(vSphere Replication)**

3.Virtual Machines
  - **acct-app-01**
  - **acct-db-01**
  - **acct-web-01**

4.Recovery Plan
  - Add to new recovery plan: **Accounting Recovery Plan**
  - description: **Accounting department Recovery Plan**

## 新增 Site-Wide Recovery Plan
1.Name and direction
  - Name: **Site-Wide Recovery Plan**
  - Description: **Recovery plan for the entire RegionA datacenter**

2.Protection Groups
  - **Accounting Protection Group**
  - **Introduction Protection Group**
  - **Sales Application Protection Group**

3.Test Networks
  - **Use site-level mapping** (dafault)

## 增加sales-web-01複寫保護
- VM: **sales-web-01**
- Target site: **vcsa-01b.corp.local**
- Replication Sever: **vr-01b**
- Target datastore:
  - disk format: **Thin provision**
  - vm storage policy: **vSAN Default Storage policy**
  - datastore: **vsanDatastoreB01**
- Replication:
  - RPO: **4 hours**
  - network compression for VR data: **enable**
- Production Groups
  - Add to existing protection groups: **Sales Protection Group**

## 新增虛擬主機putty連線設定
- 虛擬機器 IP 位址
  - **sales-app-01**: 192.168.120.200
  - **sales-db-01**: 192.168.120.201
  - **sales-web-01**: 192.168.120.202
- 登入帳密: **root** (免密碼)
- 測試連線
 > 同意接受該主機公鑰

**備註** 使用 Web Console 連線虛擬機器帳密: **root** / **VMware1!**

## 加入命令檔
使用以下的方式將程式碼複製傳送至主控台。

> 點選主控台左上方 **傳送文字**，彈跳出 **傳送文字至主控台** 視窗。

> 將游標移至要傳送文字的位置。

> 將欲傳送的文字貼入視窗中。點選 **傳送** 開始傳送內容至剛剛游標所在位置。

![001.png](./pics/99/001.png)

## 調整Sales Application Recovery Plan
- Priority: **4(LOW)**
- Dependencies: sales-db-01 **<--** sales-app-01 **<--** sales-web-01
 > **<--**: 後方虛擬機器相依於前方虛擬機器。前方虛擬機器會比後方虛擬機器先進行啟動程序。

---
[[回到主頁]](./README.md)

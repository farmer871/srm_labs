#!/bin/bash
#
# edited by semigod
#
# 2020/09/09

indexer1 () {
  if [ $# -eq 0 ]; then
    cat ${file} | sed '/```/,/```/d' | grep '^# ' | sed -e 's/^#\ //'
  else
    cat ${file} | sed '/```/,/```/d' | grep '^# ' | sed -e 's/^#\ //' -e 's/\ /-/g' -e 's/[()?？]//g'
  fi
}

indexer2 () {
  if [ $# -eq 0 ]; then
    cat ${file} | sed '/```/,/```/d' | grep '^## ' | sed -e 's/^##\ //'
  else
    cat ${file} | sed '/```/,/```/d' | grep '^## ' | sed -e 's/^##\ //' -e 's/\ /-/g' -e 's/[()?？]//g'
  fi
}

indexer3 () {
  if [ $# -eq 0 ]; then
    cat ${file} | sed '/```/,/```/d' | grep '^### ' | sed -e 's/^###\ //'
  else
    cat ${file} | sed '/```/,/```/d' | grep '^### ' | sed -e 's/^###\ //' -e 's/\ /-/g' -e 's/[()?？]//g'
  fi
}

indexer4 () {
  if [ $# -eq 0 ]; then
    cat ${file} | sed '/```/,/```/d' | grep '^#### ' | sed -e 's/^####\ //'
  else
    cat ${file} | sed '/```/,/```/d' | grep '^#### ' | sed -e 's/^####\ //' -e 's/\ /-/g' -e 's/[()?？]//g'
  fi
}

indexer5 () {
  if [ $# -eq 0 ]; then
    cat ${file} | sed '/```/,/```/d' | grep '^##### ' | sed -e 's/^#####\ //'
  else
    cat ${file} | sed '/```/,/```/d' | grep '^##### ' | sed -e 's/^#####\ //' -e 's/\ /-/g' -e 's/[()?？]//g'
  fi
}

indexer6 () {
  if [ $# -eq 0 ]; then
    cat ${file} | sed '/```/,/```/d' | grep '^###### ' | sed -e 's/^######\ //'
  else
    cat ${file} | sed '/```/,/```/d' | grep '^###### ' | sed -e 's/^######\ //' -e 's/\ /-/g' -e 's/[()?？]//g'
  fi
}


transform () {
  echo "$1" | tr '[:upper:]' '[:lower:]'
}

if [ $# -ne 1 ]; then
  echo -e "\n<!> Usage: $0 <markdown_file> <!>\n"
  exit
fi

file=$1
oldIFS=$IFS
IFS=$'\n'


originIndex1=($(indexer1))
originIndex2=($(indexer2))
originIndex3=($(indexer3))
originIndex4=($(indexer4))
originIndex5=($(indexer5))
originIndex6=($(indexer6))

index1=($(indexer1 1))
index2=($(indexer2 1))
index3=($(indexer3 1))
index4=($(indexer4 1))
index5=($(indexer5 1))
index6=($(indexer6 1))

count1=${#index1[@]}
count2=${#index2[@]}
count3=${#index3[@]}
count4=${#index4[@]}
count5=${#index5[@]}
count6=${#index6[@]}


echo -e "\n# lvl 1"
echo -e "======="
for ((i=0;i<${count1};i++)); do
  echo "- [${originIndex1[$i]}](#$(transform ${index1[$i]}))"
done

echo -e "\n# lvl 2"
echo -e "======="
for ((i=0;i<${count2};i++)); do
  echo "- [${originIndex2[$i]}](#$(transform ${index2[$i]}))"
done

echo -e "\n# lvl 3"
echo -e "======="
for ((i=0;i<${count3};i++)); do
  echo "- [${originIndex3[$i]}](#$(transform ${index3[$i]}))"
done

echo -e "\n# lvl 4"
echo -e "======="
for ((i=0;i<${count4};i++)); do
  echo "- [${originIndex4[$i]}](#$(transform ${index4[$i]}))"
done

echo -e "\n# lvl 5"
echo -e "======="
for ((i=0;i<${count5};i++)); do
  echo "- [${originIndex5[$i]}](#$(transform ${index5[$i]}))"
done

echo -e "\n# lvl 6"
echo -e "======="
for ((i=0;i<${count6};i++)); do
  echo "- [${originIndex6[$i]}](#$(transform ${index6[$i]}))"
done

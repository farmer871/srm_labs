## [深入淺出]


- [復原計劃中包含多組保護群組](#復原計劃中包含多組保護群組)

- [多組虛擬機器調整復原優先順序](#多組虛擬機器調整復原優先順序)

- [自訂復原步驟](#自訂復原步驟)

  - [命令復原步驟](#命令復原步驟)
  - [訊息提示復原步驟](#訊息提示復原步驟)
  - [應用範例](#應用範例)

- [vSphere Replication搭配使用vSAN](#vsphere-replication搭配使用vsan)

  - [設定複寫目標為vsan](#設定複寫目標為vsan)
  - [復原點目標rpo](#復原點目標rpo)
  - [保留原則(選擇性)](#保留原則選擇性)
  - [啟用客體作業系統靜止(選擇性)](#啟用客體作業系統靜止選擇性)
  - [啟用網路壓縮(選擇性)](#啟用網路壓縮選擇性)
  - [啟用複寫流量的網路加密(選擇性)](#啟用複寫流量的網路加密選擇性)

- [SRM組態管理](#srm組態管理)
  - [匯出SRM組態](#匯出srm組態)
  - [檢視SRM組態](#檢視srm組態)


### 復原計劃中包含多組保護群組
**注意以下復原計劃設計條件**

- 一組復原計劃(Recovery Plan)可以包含一組或多組保護群組(Protection Group(s))。

- ㄧ組保護群組可以包含一臺或多臺虛擬機器。

- 一臺虛擬機器僅能屬於一組保護群組。

- 當調整虛擬機器的啟動選項時，所變動的屬性便會與虛擬機器跟隨。例如: 調整虛擬機復原優先順序(priority)。

- 虛擬機器相依性，相互依存的虛擬機器必須在相同的復原優先順序。否則，復原優先順序的權重將高於虛擬機器相依性。

- 相互依存的虛擬機器可不必在相同保護群組，但必須要在同一復原計劃當中。

從下圖來看，復原計劃 **Site-Wide Recovery Plan** 包含 **3** 組保護群組。
- Accounting Protection Group
- Introduction Protection Group
- Sales Application Protection Group

![001.png](./pics/04/001.png)

當在復原計劃 **Sales Application Recovery Plan** 中調整其中虛擬機器的啟動選項(例: 復原優先順序、VM相依性......等虛擬機器相關屬性)時。當回到其他復原計劃 **Site-Wide Recovery Plan** 時，虛擬機器屬性便會跟隨，在資訊狀態清單也會於 **Status Modified By** 顯示，由那一組復原計劃進行編輯調整。

![002.png](./pics/04/002.png)

### 多組虛擬機器調整復原優先順序

選取多個虛擬機器時，可一併進行復原優先順序(Priority Group)與啟動動作(Startup Action)調整。

![003.png](./pics/04/003.png)

### 自訂復原步驟

自訂復原步驟為 [命令復原步驟](#命令復原步驟) 或 [訊息提示復原步驟](#訊息提示復原步驟)。

#### 命令復原步驟
命令復原步驟包含**頂層命令**或**每台虛擬機器命令**。
- 如果可以的話，**建議使用虛擬機器命令**。

- **頂層命令**會在 SRM Appliance 上執行。使用 SRM 命令可以開啟實體裝置的電源或重新導向網路流量。不過，**頂層命令不見得具有親和性和熟悉度**。

- 在復原程序期間，SRM 會將每台虛擬機器命令與新復原的虛擬機器相關聯。**開啟虛擬機器電源後**，可以使用虛擬機器命令完成組態工作。

#### 訊息提示復原步驟
**SRM 在復原期間透過使用者介面中顯示訊息**。
- 可使用訊息暫停復原作業，並向執行復原計劃的使用者提供資訊。

- **訊息提示**可指示使用者**執行手動復原工作**或**驗證步驟**。

- 若要直接回應提示，使用者可以採取的**唯一動作是關閉訊息(DISMISS)**，動作解除後將可繼續進行復原動作。

#### 應用範例1
以下採用 **虛擬機器命令** 和 **訊息提示** 作一基本應用範例。

- 復原過程中，虛擬機器 **sales-app-01** 開機後(Post Power on Steps)需要進行驗證。

- 會寫入一筆登入訊息 `hello, world`。

 ![004.png](./pics/04/004.png)

 程式碼範例: hello.sh

 ```bash
 #!/bin/bash

 echo "hello, word - $(date +%Y-%m-%d_%T)" > $HOME/hello.world
 ```

- 確認 httpd 服務必須正常啟用。

 ![005.png](./pics/04/005.png)

 程式碼範例: check.sh

 ```bash
 #!/bin/bash

 echo "$ log start" > $HOME/httpd.log
 httpdStatus=$(systemctl is-active httpd)
 while [ "${httpdStatus}" != "active" ]; do
   echo "httpd is not ready at $(date +%Y-%m-%d_%T)" >> $HOME/httpd.log
   sleep 30

   httpdStatus=$(systemctl is-active httpd)
 done

 echo "httpd is OK! at $(date +%Y-%m-%d_%T)" >> $HOME/httpd.log

 exit
 ```

- 相關動作完成後，SRM 顯示訊息提醒災備處理小組進行再次驗證。

```text
please login sales-app-01 to check the status of httpd
# cat /root/httpd.log
# systemctl status httpd
```

 ![006.png](./pics/04/006.png)

按照復原計劃需求，建立 **自訂復原步驟**。

![007.png](./pics/04/007.png)

進行復原計劃測試驗證，**復原計劃要使其完善，進行數次測試並調整是必要的**！

觀察復原步驟進行。

![009.png](./pics/04/009.png)

等.等..等...

![010.png](./pics/04/010.png)

訊息提示產生。若完成提醒項目，可點選 **DISMISS** 解除暫停，SRM 將換繼續完成復原計劃動作。

![011.png](./pics/04/011.png)

連線虛擬機器 **sales-app-01** 進行驗證。

![012.png](./pics/04/012.png)

復原計劃測試達成。

![013.png](./pics/04/013.png)

透過復原步驟確認相關流程。如果其中的程序執行或確認失敗，復原動作將會終止，並停止在發生錯誤的復原步驟。**請再次調整後進行復原測試**。

![014.png](./pics/04/014.png)

#### 應用範例2

修改虛擬機器 **sales-app-01** 復原屬性。

![015.png](./pics/04/015.png)

調整 **sales-app-01** 設定，將應用範例1的 `check.sh` 替換成以下程式範例 `check_error.sh`。

![016.png](./pics/04/016.png)

程式碼範例: check_error.sh

```bash
#!/bin/bash

echo "$ log start" > $HOME/httpd.log
httpdStatus=$(systemctl is-active httpd)
while [ "${httpdStatus}" != "active" ]; do
  echo "httpd is not ready at $(date +%Y-%m-%d_%T)" >> $HOME/httpd.log
  sleep 10

  httpdStatus=$(systemctl is-active httpd)

  timeout=$(expr ${timeout} + 1)
  if [ ${timeout} -eq 3 ]; then
    echo "timeout@ at $(date +%Y-%m-%d_%T)" > $HOME/httpd.error
    exit 188
  fi
done

echo "httpd is OK! at $(date +%Y-%m-%d_%T)" >> $HOME/httpd.log

exit
```

使用 putty 連線 **sales-app-01**，關閉 httpd 服務。輸入以下命令完成。

```bash
# systemctl disable httpd
# systemctl stop httpd
```

![017.png](./pics/04/017.png)

請執行復原計畫測試，看看結果如何？
**注意** 記得勾選 **Replicate recent changes to recovery site**

![018.png](./pics/04/018.png)

結果當然是**不會成功復原**！而且整個復原計畫測試會因此中斷。

不過這也是我們預期的結果，因為在 **sales-app-01** 停止了 httpd 服務，且修改的程式會偵測服務，如果遇到錯誤會回傳值 `188`。

![019.png](./pics/04/019.png)

讓我們檢視一下錯誤訊息，的確出現我們指定的錯誤回傳值。

![020.png](./pics/04/020.png)



![021.png](./pics/04/021.png)


### vSphere Replication搭配使用vSAN

#### 設定複寫目標為vSAN
**sales-web-01** 的複寫目標也可以使用 vSAN。

> 點選 Disk Format: **Thin provision**

> 點選 VM storage policy: **vSAN Default Storage Policy**

> 選擇 **vsanDatastoreB01**

![vsan-001.png](./pics/04/vsan/001.png)

接著來了解有關複寫選項。

#### 復原點目標RPO
複寫組態設定復原點目標 (Recovery Point Objective, RPO) 值，可以決定容許的資料遺失上限。

RPO 值越小，復原中遺失的資料越少，但是將複本保持為最新狀態會耗用更多的網路頻寬。RPO 值會影響複寫排程，但 vSphere Replication 不會遵守嚴格的複寫排程。

- 將 RPO 設定為 15 分鐘，即指示 vSphere Replication 可容許遺失資料的時間長達 15 分鐘。這並不表示資料會每 15 分鐘複寫一次。
- 假定複寫組態將 **RPO 設定為 15 分鐘**。**複寫同步在 12:00 開始**，且**需要 5 分鐘傳送到目標站台**。表示虛擬機器到了 12:05 就可以在目標站台使用，但其複寫資料為 12:00 的狀態。**下一次同步最遲不會超過 12:10 開始**， 因為目標站台會在 12:15 完成複寫同步，這樣一來目標站台在 12: 15 若要使用複寫資料，其狀態為 12:10 的，與最早複寫同步 12:00 狀態相差 10 分鐘，並沒有引起 RPO 違規。
- 如果複寫於 12:00 開始，且需要 10 分鐘傳輸複寫資料，則複寫將於 12:10 完成。立即啟動另一個複寫，但將在 12:20 完成。在**時間間隔 12:15-12:20 期間就會發生了 RPO 違規**，因為最新的可用複寫的虛擬機器狀態是 12:00 的。
- 複寫排程器嘗試透過重疊複寫以最佳化頻寬使用量來滿足這些限制。
- **選取作業系統靜止(Guest OS quiescing)選項後，就不支援低於 15 分鐘的 RPO**。

![vsan-002.png](./pics/04/vsan/002.png)

#### 保留原則(選擇性)
設定複寫時，可以啟用從多個時間點 (Multiple Points in Time, MPIT) 保留最多 24 個 VM 複本資料。**復原虛擬機器後，可將其還原為特定快照**。

- 若未設定保留原則，當建立新的執行個體時，先前的執行個體會到期，且 vSphere Replication Server 將刪除它。
- vSphere Replication 保留的複寫執行個體數量取決於設定的保留原則，但是也會要求 RPO 期間足夠短，以便建立這些執行個體。
- vSphere Replication 不會驗證 RPO 設定是否會建立足夠要保留的執行個體，也不會在執行個體不足時顯示警告訊息，因此管理員必須確保將 vSphere Replication 設定為建立要保留的執行個體。

![vsan-003.png](./pics/04/vsan/003.png)

假設組態範例: RPO 為 4 小時。希望可以每天保留過去 5 天，每天 6 個複寫資料 。

> 對於保留政策每天 6 個複寫執行個體，可以達成。

> 但總數 30 個複寫執行個體已經超過 24 個的限制。

![vsan-004.png](./pics/04/vsan/004.png)

但將每天保存數提高至 7 個時，系統會顯示告警資訊。

![vsan-005.png](./pics/04/vsan/005.png)

#### 啟用客體作業系統靜止(選擇性)
vSphere Replication 可保證屬於某個虛擬機器的所有磁碟之間的當機一致性。如果使用靜止，則可能會取得更高層級的一致性。可用的靜止類型由虛擬機器的作業系統決定。如需 Windows 和 Linux 虛擬機器的靜止支援資訊，請參閱 [相容列表](#https://docs.vmware.com/tw/vSphere-Replication/8.3/rn/vsphere-replication-compat-matrix-8-3.html)。

![vsan-006.png](./pics/04/vsan/006.png)

#### 啟用網路壓縮(選擇性)
壓縮透過網路傳輸的複寫資料會儲存網路頻寬，而且可能會協助減少 vSphere Replication Server 上使用的緩衝區記憶體數量。但是，壓縮與解壓縮資料需要來源站台與管理目標資料存放區的伺服器上具有更多 CPU 資源。

![vsan-007.png](./pics/04/vsan/007.png)

#### 啟用複寫流量的網路加密(選擇性)
可以為新的及現有複寫啟用複寫流量資料的網路加密，以增強資料傳輸的安全性。
如果設定已加密虛擬機器的複寫，此選項會自動開啟，且無法停用。

![vsan-008.png](./pics/04/vsan/008.png)

請自行完成後續複寫保護設定。

![vsan-009.png](./pics/04/vsan/009.png)

自行測試復原計畫是否可以正確執行。

---

[使用和設定 vSphere Replication 的最佳做法](https://docs.vmware.com/tw/vSphere-Replication/8.3/com.vmware.vsphere.replication-admin.doc/GUID-C89FE68A-2635-42DD-B1B3-EFE1BE71DB76.html)

---

### SRM組態管理
可以使用 SRM Configuration Import/Export Tool 匯出和匯入組態資料。

如果計劃將 SRM 移轉到不同的主機，則可以使用此工具將詳細目錄對應、復原計劃、保護群組，以及相關物件匯出至 XML 檔案。接著可以從先前匯出的檔案匯入組態資料。

可透過 Site Recovery 使用者介面和獨立 .zip 封存檔使用 VMware SRM Configuration Import/Export Tool。
~~如果您使用的是 Site Recovery Manager 應用裝置，則該工具也會隨該應用裝置一起部署。您可以從 [VMware SRM 下載](https://my.vmware.com/en/group/vmware/info?slug=infrastructure_operations_management/vmware_site_recovery_manager/8_1#drivers_tools) 頁面下載獨立工具。~~

#### 匯出SRM組態
使用 SRM 管理界面中的 UI 界面，就可以將現有 SRM 組態匯出成檔案。
選擇 **Site Pair** > **Summary** ，點選 **Site Recovery Manager** 部份的 **EXPORT/IMPORT SRM CONFIGURATION**，選擇 **Export** 進行匯出組態動作。

![022.png](./pics/04/022.png)

接著下載匯出的 SRM 組態資料。

![023.png](./pics/04/023.png)

#### 檢視SRM組態
匯出的SRM組態為 XML 格式。可以使用瀏覽器檢視會比較清楚。
使用瀏覽器搜尋**復原計畫**類型 `Type="RecoveryPlans">` 。

![024.png](./pics/04/024.png)

與 SRM 管理界面的 **復原計畫** 進行比較。

![025.png](./pics/04/025.png)

使用瀏覽器搜尋**保護群組**類型 `Type="ProtectionGroups">` 。

![026.png](./pics/04/026.png)

與 SRM 管理界面的 **保護群組** 進行比較。

![027.png](./pics/04/027.png)

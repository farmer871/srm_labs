# 使用 SRM 演練虛擬機器故障移轉

## [目的]
無需任何深入的配置要求即可體驗 SRM 的某些功能。

按照相關步驟進行可完成以下課題。
- 將虛擬機器(intro-app-01)進行計劃性遷移從主要資料中心(RegionA)至災難備援中心(RegionB)。
- 成功完成故障移轉後，使用 SRM 的重新保護(re-protect)並再次回復(failback)至主要資料中心。

## [步驟大綱]

[故障移轉虛擬機](#故障移轉虛擬機)
- [連線 vSphere Client](#連線-vsphere-clinet)
- [連線 SRM 確認複寫機制狀態](#連線-srm-確認複寫機制狀態)
- [檢視已複寫虛擬機器](#檢視已複寫虛擬機器)
- [檢視指定已複寫虛擬機器](#檢視指定已複寫虛擬機器)
- [選擇復原計畫](#選擇復原計畫)
- [執行復原計畫](#執行復原計畫)
- [復原選項確認選擇](#復原選項確認選擇)
- [開始故障移轉](#開始故障移轉)
- [監控復原過程](#監控復原過程)
- [復原完成](#復原完成)
- [檢視復原步驟狀態](#檢視復原步驟狀態)
- [在 RegionA 使用 vSphere Web Client 檢視虛擬機狀態](#在-regiona-使用 vsphere-web-client-檢視虛擬機狀態)
- [在 RegionB 檢視復原之虛擬機狀態](#在-regionb-檢視復原之虛擬機狀態)
- [從複寫角度檢視復原之虛擬機狀態](#從複寫角度檢視復原之虛擬機狀態)

[重新保護虛擬機與容錯回復](#重新保護虛擬機與容錯回復)
- [重新保護虛擬機器](#重新保護虛擬機器)
- [確認重新保護選項](#確認重新保護選項)
- [監控重新保護流程](#監控重新保護流程)
- [vSphere複寫狀態](#vsphere複寫狀態)
- [容錯回復](#容錯回復)
- [確認虛擬機已復原完成](#確認虛擬機已復原完成)

## [執行步驟]

### 故障移轉虛擬機

#### 連線 vSphere Clinet

開啟Chrome瀏覽器。

![001.png](./pics/01/001.png)

確認連線至 **RegionA vCenter Server** (vcsa-01a.corp.local)。

![002.png](./pics/01/002.png)

**第一種登入方式**: 勾選 **Use Windows session authentication** ，再點選 **Login** 登入。

![003.png](./pics/01/003.png)

**第二種登入方式**: 先將滑鼠點擊選取主控台**使用者欄位**，再點選上方**附註**，同樣選擇**使用者名稱**，點選旁邊的**傳送**圖示，就會自動將其中資訊傳送至下方主控台**使用者名稱**欄位。
密碼部份也依此方式操作完成。後續點擊**Login**登入。

![004.png](./pics/01/004.png)

**附註** RegionA vCenter Server也可以透過下列方式選取。

![005.png](./pics/01/005.png)

#### 連線 SRM 確認複寫機制狀態
開啟災難復原管理員(Site Recovery Manager, **SRM**)。
> 點選 **Menu** > **Site Recovery**。

![006.png](./pics/01/006.png)

> 點選 **OPEN Site Recovery**。

會開啟另一個分頁進入SRM。

![007.png](./pics/01/007.png)

這裡可以看到 **站點配對** 資訊，目前有 **vcsa-01a <--> vcsa-01b**。

> 點選 **VIEW DETAILS** 進入。

![008.png](./pics/01/008.png)

從站點配對(Site Pair)摘要資訊，可以檢視目前 **SRM** 與 **vSphere Replication** 運作狀態。
![009.png](./pics/01/009.png)

#### 檢視已複寫虛擬機器

> 點選 **Replications** > **Outgoing**，檢視 **RegionA -> RegionB** 方向的複寫狀態。

![010.png](./pics/01/010.png)

#### 檢視指定已複寫虛擬機器

> 選擇 **intro-app-01** 並展開顯示詳細資訊。

![011.png](./pics/01/011.png)

從 vSphere Web Client 確認 **intro-app-01** 已成功複寫，可進行故障轉移。

![012.png](./pics/01/012.png)

#### 選擇復原計畫

> 點選 **Recovery Plans**，選擇 **Introduction to Site Recovery** 這個復原計畫。

![013.png](./pics/01/013.png)

> 點選上方 `‧‧‧` 展開其他選項，選擇 **保護群組(Protection Groups)**。

![014.png](./pics/01/014.png)

目前使用的保護群組是 **Introduction Protection Group**，狀態正常。

![015.png](./pics/01/015.png)

> 點選上方 `‧‧‧` 展開選項，選擇(被保護的) **虛擬機器(Virrual Machines)**。

![014.png](./pics/01/014.png)

目前使用此復原計畫的虛擬機器是 **intro-app-01**，屬於保護群組 **Introduction Protection Group**，目前可進行復原。

![016.png](./pics/01/016.png)

#### 執行復原計畫

> 點選 `RUN` 執行(計劃性)復原計畫。

![017.png](./pics/01/017.png)

#### 復原選項確認選擇

> 彈跳出復原動作確認選項，請都勾選 **Recovery confirmation**選項，確認進行復原動作。並選擇 **復原類型(Recovery Type)** 為 **計劃性遷移(Planned Migration)**。點選 **NEXT** 繼續。

採用**計劃性遷移**復原類型，若在復原階段遇到問題，在站點及複寫機制可用時，可取消進行之復原計畫。

![018.png](./pics/01/018.png)

#### 開始故障移轉

> 若復原資訊確認，點選 **FINISH** 進行復原動作。

![019.png](./pics/01/019.png)

#### 監控復原過程

> 點選 **復原步驟(Recovery Steps)** 監控復原過程。

![020.png](./pics/01/020.png)

#### 復原完成

![021.png](./pics/01/021.png)

#### 檢視復原步驟狀態

![022.png](./pics/01/022.png)

#### 在 RegionA 使用 vSphere Web Client 檢視虛擬機狀態

使用 **vSphere Web Client** 連接 **RegionA vCenter Server**。

> 點選 **Menu** > **VMs and Templates**

![023.png](./pics/01/023.png)

> 點選 **vcsa-01a.corp.local** > 資料夾 **Protected Apps RegionA01**

虛擬機 intro-app-01 是 **電源關閉** 狀態。

![024.png](./pics/01/024.png)

#### 在 RegionB 檢視復原之虛擬機狀態

> 點選 **vcsa-01b.corp.local** > 資料夾 **Recovered Apps RegionA01**

確認虛擬機 intro-app-01 是 **電源開啟** 狀態。

![025.png](./pics/01/025.png)

取得 intro-app-01 的 IP 位址為 **192.168.120.101**。

![026.png](./pics/01/026.png)

使用 **Putty** 工具 SSH 連線至 **intro-app-01**。測試與 **vcsa-01a.corp.local** 網路連線正常。

![027.png](./pics/01/027.png)

![028.png](./pics/01/028.png)

#### 從複寫角度檢視復原之虛擬機狀態

![029.png](./pics/01/029.png)


### 重新保護虛擬機與容錯回復
重新保護(Re-protect)與容錯回復(Failback)

#### 重新保護虛擬機器
再次回到 SRM 管理界面中的復原計畫 **Introduction to Site Recovery**。

> 點選 **Recovery Plans** > **Summary**，點選 `‧‧‧` 展開並選擇 **Reprotect** 進行重新保護。

![030.png](./pics/01/030.png)

或是

> 點選 **Recovery Plans** > **Recovery Steps**，點選 **REPROTECT** 進行重新保護。

![031.png](./pics/01/031.png)

#### 確認重新保護選項

> 勾選 **重新保護** 確認，了解此操作無法重來！點選 **NEXT** 繼續。

![032.png](./pics/01/032.png)

> 點選 **FINISH** 開始進行重新保護。

![033.png](./pics/01/033.png)

#### 監控重新保護流程

![034.png](./pics/01/034.png)

當重新保護完成時，回到復原計畫摘要，可檢視該復原計畫狀態**已就緒**。且保護的方向已是 **RegionB01 -> RegionA01**。

![035.png](./pics/01/035.png)

#### vSphere複寫狀態

> 點選 **Replications** > **Incoming**。

因為目前複寫方向為 **RegionB -> RegionA**，所以在 **vcsa-01a.corp.local** 角度為 **寫入複寫**。確認 intro-app-01 複寫狀態正常。

![036.png](./pics/01/036.png)

#### 容錯回復
若要將受保護站台與復原站台的組態還原至其初始組態後再進行復原，可以執行容錯回復。

**SRM 容錯回復程序**。

![037.png](./pics/01/037.png)

請根據以上計劃性遷移的步驟進行復原計畫，將保護的虛擬機再次移轉至 RegionA。

![038.png](./pics/01/038.png)

確認移轉選項。

![039.png](./pics/01/039.png)

#### 確認虛擬機已復原完成

復原計畫執行完成。

![040.png](./pics/01/040.png)

依相同方式驗證虛擬機 intro-app-01 已順利移轉並開啟電源。

![041.png](./pics/01/041.png)

若要回復成原有組態，請自行完成 **重新保護** 操作。


## [參考]

- VMware Hands-on Labs - **HOL-2005-01-SDC**: VMware Site Recovery Manager - Data Center Migration and Disaster Recovery
   - [啟動實驗](http://labs.hol.vmware.com/HOL/catalogs/lab/6228 "啟動實驗")
   - [實驗指引](https://docs.hol.vmware.com/HOL-2020/hol-2005-01-sdc_html_en/ "實驗指引")

---
[[回到主頁]](./README.md)
